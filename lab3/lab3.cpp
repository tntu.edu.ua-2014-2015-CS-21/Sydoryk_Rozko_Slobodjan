#include <iostream>
#include <fstream> 
using namespace std;
 
 
void print(int arr[], int d) {
 
     for (int i = 0; i <d; i++) 
     {
         cout << arr[i] << "-";
     }
     cout << endl;
}
 
 
void quickSort(int arr[], int left, int right) { 
 
     int i = left, j = right;
     int tmp;
     int pvt = arr[(left + right) / 2];
 
 
     while (i <= j) {
           while (arr[i] < pvt)
                 i++;
           while (arr[j] > pvt)
                 j--;
           if (i <= j) {
           tmp = arr[i];
           arr[i] = arr[j];
           arr[j] = tmp;
           i++;
           j--;
           }
     }
 
     if (left < j)
     quickSort(arr, left, j); 
     if (i < right)
     quickSort(arr, i, right);
 
}
 
int main ()
{
 
    ifstream F1; 
    ofstream F2;
 
    F1.open("input.txt"); 
	int d;
	F1>>d; 
    cout<<"Size: "<<d<<endl;
 
    int i;
    int *arr=new int [d]; 
 
    if (F1) 
    {
       while (!F1.eof()) 
       {          	  
          for(i=0; !F1.eof(); i++)
          {
            F1>>arr[i];
            cout<< "Array["<< i+1 << "]:"<<arr[i]<<endl;  
 
          }
 
       }     
    }
    F1.close(); 
 
    print(arr,d); 
    quickSort(arr, 0, d-1);
 
 
    F2.open("print.txt");
    for (i=0; i<d; i++)
    {
        F2<<arr[i]<<"-";   
    }
    F2.close();
 
    print(arr,d); 
    return 0;
 
}
