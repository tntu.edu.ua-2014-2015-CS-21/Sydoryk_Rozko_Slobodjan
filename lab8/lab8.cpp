#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <list>

using std::vector;
using std::map;
using std::list;
using std::ifstream;
using std::cout;
using std::endl;


const char FILENAME[] = "SCC.txt";


long get_node_count(const char filename[]);
vector< vector<long> > parse_file(const char filename[]);
map< long, vector<long> > compute_scc(vector< vector<long> > &graph);
vector< vector<long> > reverse_graph(const vector< vector<long> > &graph);
void dfs_loop(const vector< vector<long> > &graph, vector<long> &finishTime, vector<long> &leader);
long dfs(const vector< vector<long> > &graph, long nodeIndex, vector<bool> &expanded, vector<long> &finishTime, long t, vector<long> &leader, long s);
list<unsigned long> get_largest_components(const map< long, vector<long> > scc, long size);



int main() {
    vector< vector<long> > graph = parse_file(FILENAME);

    map< long, vector<long> > scc = compute_scc(graph);

    list<unsigned long> largestComponents = get_largest_components(scc, 5);

    list<unsigned long>::iterator it;
    for (it = largestComponents.begin(); it != largestComponents.end(); it++) {
        cout << *it << ' ';
    }
    cout << endl;
    return 0;
}



vector< vector<long> > parse_file(const char filename[]) {
    long nodeCount = get_node_count(filename);
    vector< vector<long> > graph(nodeCount);

    ifstream graphFile(filename);

    long nodeIndex;
    long outIndex;

    while (graphFile) {
        graphFile >> nodeIndex;
        graphFile >> outIndex;

        graph[nodeIndex - 1].push_back(outIndex - 1);
    }
graphFile.close();
    return graph;
}


long get_node_count(const char filename[]) {
    ifstream graphFile(filename);

    long maxNodeIndex = 0;
    long nodeIndex = 0;

    while (graphFile) {
        graphFile >> nodeIndex;

        if (nodeIndex > maxNodeIndex) {
            maxNodeIndex = nodeIndex;
        }

        graphFile >> nodeIndex;

        if (nodeIndex > maxNodeIndex) {
            maxNodeIndex = nodeIndex;
        }
    }

    return maxNodeIndex;
}


map< long, vector<long> > compute_scc(vector< vector<long> > &graph) {
    vector<long> finishTime(graph.size(), 0);
    vector<long> leader(graph.size(), 0);

    vector<long>::iterator it;
    long index = 0;
    for (it = finishTime.begin(); it != finishTime.end(); it++) {
        *it = index;
        index++;
    }

    vector< vector<long> > reversed = reverse_graph(graph);
    dfs_loop(reversed, finishTime, leader);

    dfs_loop(graph, finishTime, leader);

    map< long, vector<long> > scc;
    vector<long>::iterator lit;

    for (lit = leader.begin(); lit != leader.end(); lit++) {
        long nodeIndex = lit - leader.begin();

        scc[*lit].push_back(nodeIndex);
    }

    return scc;
}



vector< vector<long> > reverse_graph(const vector< vector<long> > &graph) {
    vector< vector<long> > reversed(graph.size());

    vector< vector<long> >::const_iterator it;
    for (it = graph.begin(); it != graph.end(); it++) {
        long nodeIndex = it - graph.begin();

        vector<long>::const_iterator eit;
        for (eit = graph[nodeIndex].begin(); eit != graph[nodeIndex].end(); eit++) {
            reversed[*eit].push_back(nodeIndex);
        }
    }

    return reversed;
}



void dfs_loop(const vector< vector<long> > &graph, vector<long> &finishTime, vector<long> &leader) {
    vector<bool> expanded(graph.size(), 0);
    vector<long> loopFinishTime = finishTime;

    long t = 0;
    vector<long>::reverse_iterator it;


    for (it = loopFinishTime.rbegin(); it != loopFinishTime.rend(); it++) {
        if (!expanded[*it]) {
            t = dfs(graph, *it, expanded, finishTime, t, leader, *it);
        }
    }
}


long dfs(
    const vector< vector<long> > &graph,
    long nodeIndex,
    vector<bool> &expanded,
    vector<long> &finishTime,
    long t,
    vector<long> &leader,
    long s
) {
    expanded[nodeIndex] = true;

    leader[nodeIndex] = s;

    vector<long>::const_iterator it;
    for (it = graph[nodeIndex].begin(); it != graph[nodeIndex].end(); it++) {
        if (!expanded[*it]) {
            t = dfs(graph, *it, expanded, finishTime, t, leader, s);
        }
    }

    finishTime[t] = nodeIndex;
    t++;

    return t;
}


list<unsigned long> get_largest_components(const map< long, vector<long> > scc, long size) {
    list<unsigned long> largest(size, 0);

    map< long, vector<long> >::const_iterator it;
    for (it = scc.begin(); it != scc.end(); it++) {
        list<unsigned long>::iterator lit;
        for (lit = largest.begin(); lit != largest.end(); lit++) {
            if (*lit < it->second.size()) {
                largest.insert(lit, it->second.size());
                largest.pop_back();
                break;
            }
        }
    }

    return largest;
}
