#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
/*
 * Complete the function below.
 */

 int main() {
    FILE *f = fopen(getenv("OUTPUT_PATH"), "w");
    int res;

    int _arr_size, _arr_i;
    scanf("%d\n", &_arr_size);
    int _arr[_arr_size];
    for(_arr_i = 0; _arr_i < _arr_size; _arr_i++) {
        int _arr_item;
        scanf("%d", &_arr_item);

        _arr[_arr_i] = _arr_item;
    }

    res = maxDifference(_arr_size, _arr);
    fprintf(f, "%d\n", res);

    fclose(f);
    return 0;
}

