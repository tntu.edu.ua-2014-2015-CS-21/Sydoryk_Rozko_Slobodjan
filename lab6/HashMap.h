const int TABLE_SIZE = 1000000;
#pragma once
#include "HashEntry.h"
#include <iostream>
#include <list>
using namespace std;

class HashMap {

private:
	list<__int64> vals;
	HashEntry **table;

public:

	HashMap() {

		table = new HashEntry*[TABLE_SIZE];
		for (int i = 0; i < TABLE_SIZE; i++)

			table[i] = NULL;

	}



	int get(int key) {

		int hash = (key % TABLE_SIZE);

		while (table[hash] != NULL && table[hash]->getKey() != key)

			hash = (hash + 1) % TABLE_SIZE;

		if (table[hash] == NULL)

			return -1;

		else

			return table[hash]->getValue();

	}



	void put(int key, int value) {

		int hash = (key % TABLE_SIZE);

		while (table[hash] != NULL && table[hash]->getKey() != key)

			hash = (hash + 1) % TABLE_SIZE;

		if (table[hash] != NULL)

			delete table[hash];

		table[hash] = new HashEntry(key, value);
		vals.push_back(value);

	}
	bool search(int y){
		int i;
		bool out = false;
		if (find(begin(vals), end(vals), y) != end(vals)) { out = true; }
		return out;
	}



	~HashMap() {

		for (int i = 0; i < TABLE_SIZE; i++)

		if (table[i] != NULL)

			delete table[i];

		delete[] table;

	}

};
